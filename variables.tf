variable "location" {
  description = "(Required) The location/region where the core network will be created. The full list of Azure regions can be found at https://azure.microsoft.com/regions"
  default     = "eastus2"
  type = string
}

variable "sku" {
  description = "Sku of the lb, options are Basic and Standard"
  default = "Standard"
  type = string
}

variable "resource_group_name" {
  description = "(Required) The name of the resource group where the load balancer resources will be placed."
  type = string
}

variable "tags" {
  type = map(string)

  default = {
    source = "terraform"
  }
}

variable "name" {
  description = "(Required) Default prefix to use with your resource names."
  default     = "azure_lb"
  type = string
}

variable "frontend" {
  description = "Frontend ip configureation"
  type = map(any)
}  

variable "backend_pool_names" {
  description = "List of backend pool name, each name will create a seperate backend pool for the lb"
  type = list(string)
}

variable "azlb_probes" {
  description = "Map of probes to assign to the lab"
  type = map(any)
}

variable "azlb_rules" {
  description = "lbs to create"
  type = map(any)
}  

